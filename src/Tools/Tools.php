<?php
    namespace techniktomcz\stagcli\Tools;

    use Symfony\Component\Console\Helper\Table;
    use Symfony\Component\Console\Output\OutputInterface;

    class Tools
    {
        public static function renderTable(OutputInterface $output, array $header, array $data)
        {
            $table = new Table($output);
            $table->setHeaders($header)->setRows($data);
            $table->render();
        }
    }
